<?php

defined('BASEPATH') OR exit ('No direct script acess allowed');
include_once APPPATH.'libraries/ClassroomAPI.php';

// ID CLiente: 879676212629-rc7f2ra0h24o38doqtp1ue30gmonn9kt.apps.googleusercontent.com
// Cliente Secret: Jc4Oli9HDxfYMHBirWChLTPT

    class Classroom extends MY_Controller{

        
        public function index(){
            $this->load->model('ClassroomModel', 'model');
            
            $v['header_content'] = $this->load->view('home/logo_home', '', true);
            $v['infos'] = $this->load->view('home/infos', '', true);
            $html = $this->load->view('common/layout', $v, true);

            $this->show($html);
        }

        public function sobre()
        {
            $this->load->model('ClassroomModel', 'model');

            $v['header_content'] = $this->load->view('sobre/titulo', '', true);
            $v['infos'] = $this->load->view('sobre/conteudo', '', true);
            $html = $this->load->view('common/layout', $v, true);

            $this->show($html);
        }

        public function funcionamento()
        {
            $this->load->model('ClassroomModel', 'model');

            // $this->model->connect();

            $v['header_content'] = $this->load->view('funcionamento/func_header', '', true);
            $v['infos'] = $this->load->view('funcionamento/func_content', '', true);
            $html = $this->load->view('common/layout', $v, true);

            $this->show($html);
        }

        public function relatorio()
        {
            $this->load->model('ClassroomModel', 'model');

            $v['header_content'] = $this->load->view('relatorio/titulo', '', true);
            $v['infos'] = $this->load->view('relatorio/conteudo', '', true);
            $html = $this->load->view('common/layout', $v, true);

            $this->show($html);
        }
        
    }
?>