<nav class="navbar navbar-extend-lg navbar-dark indigo lighten-1 mb-4">
  <a class="navbar-brand" href="<?= base_url('') ?>">
    <img src=" <?= base_url('assets/images/logo_google.png') ?> " class="logo_img img-fluid" alt="Logo Google">
  </a>
  <h2 class="text-white" >Google Classroom</h2>
  <button class="navbar-toggler second-button" type="button" data-toggle="collapse" data-target="#navbarSupportedContent23"
    aria-controls="navbarSupportedContent23" aria-expanded="false" aria-label="Toggle navigation">
    <div class="animated-icon2"><span></span><span></span><span></span><span></span></div>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent23">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('') ?>">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('classroom/sobre') ?>">Sobre</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('classroom/relatorio') ?>">Relatório</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('classroom/funcionamento') ?>">Funcionamento</a>
      </li>
    </ul>
  </div>
</nav>
