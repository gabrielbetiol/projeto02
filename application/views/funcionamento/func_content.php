<div class="row">
    <div class="col-sm-12 col-md-4 mx-auto">
        <div class="card">
            <br><br>
            <img src="<?= base_url('assets/images/google_classroom.png') ?>" class="img-fluid" alt="Google Classroom">
            <br><br>
            <a href="<?=base_url()?>googlelogin/login">
                <img src="<?= base_url('assets/images/google_signin.png') ?>" class="img-fluid" alt="Botão Google">
            </a>
        </div>
    </div>
</div>