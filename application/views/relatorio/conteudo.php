<div class="row">
    <div class="col-md-6 col-sm-12">
        <p>Sem dúvida nenhuma o Google Classroom é uma ferramente poderosa e bastante interessante.
            Possibilitando a criação de cursos, edição de informações, criação de anúncios, uma espécia de mural
            de avisos possibilitando todos os presentes a verem tais informações, distribuição de notas,
            e muitas outras funcionalidades.</P>
        <p>Outra funcionalidade bastante interessante é o professor do curso poder criar documentos onde 
            todos podem editar e visualizar ao mesmo tempo, possibilitando uma maior interação entre os alunos e professores.
            Outro ponto bastante interessante, é que o Google Classroom não necessita instalação de aplicativos,
            claro que possui um aplicativo para mobile, porém, seu acesso pode ser efetuado totalmente através do browser
        </p>
        <p>Cada vez mais a tecnologia se torna presente na vida das pessoas, ao meu ver o Google Classroom
            não está tão presente para todos, mas sem dúvida que em um futuro bem próximo essa ferramenta 
            seja bastante usada por todos. </p>
    </div>
    <div class="col-md-6 col-sm-12">
        <img src="<?= base_url('assets/images/google-classroom.jpg') ?>" class="img-fluid" alt="Google Classroom">
    </div>
</div>