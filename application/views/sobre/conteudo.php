<div class="row">
    <div class="col-sm-8 col-md-8 mb-5">
        <div class="accordion md-accordion accordion-3 z-depth-1-half" id="accordionEx194" role="tablist"
        aria-multiselectable="true">
            <h4 class="py-4 px-3">O que é o Google Classroom?</h4>
            <p class="py-4 px-3">O Google Sala de aula é um controle de missão para suas turmas. 
                Como um serviço gratuito para professores e alunos, você pode criar turmas, 
                distribuir tarefas, enviar comentários e ver tudo em um só lugar. 
                Rápido. Sem papel. Fácil.</p>

            <hr>
            <h2 class="text-center">Sobre a Classroom API</h2>
            <hr class="mb-0">

            <h4 class="py-4 px-3">Visão geral da Classroom API</h4>
            <p class="py-4 px-3">As escolas e empresas de tecnologia podem usar a Classroom API 
                para desenvolver ferramentas que interagem com o Google Sala de aula e o 
                G Suite for Education e fazer o Google Sala de aula funcionar melhor de acordo com as 
                necessidades delas. A Classroom API é uma API do Google para desenvolvedores. 
                Isso significa que os serviços que não são do Google podem aproveitar as ferramentas 
                e a infraestrutura do Google.</p>
            <hr class="mb-0">

            <!-- Item 1 -->
            <div class="card">
                <!-- Titulo -->
                <div class="card-header" role="tab" id="heading4">
                    <a data-toggle="collapse" data-parent="#accordionEx194" href="#collapse4" aria-expanded="true"
                    aria-controls="collapse4">
                    <h5 class="mb-0">
                    Quem pode usar a Classroom API? 
                    <div class="animated-icon1 float-right mt-1">
                        <span class="info-color"></span><span class="info-color"></span><span class="info-color"></span>
                    </div>
                    </h5>
                    </a>
                </div>
            <!-- Conteudo -->
                <div id="collapse4" class="collapse " role="tabpanel" aria-labelledby="heading4" data-parent="#accordionEx194">
                    <div class="card-body pt-3">
                    <p>Administradores e desenvolvedores de terceiros podem usar a Classroom API. 
                        Professores e alunos podem autorizar aplicativos de terceiros.</p>
                    </div>
                </div>
            </div>

            <!-- Item 2 -->
            <div class="card">
            <!-- Título -->
                <div class="card-header" role="tab" id="heading5">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx194" href="#collapse5" aria-expanded="false"
                    aria-controls="collapse5">
                    <h5 class="mb-0">
                    O que você pode fazer com a Classroom API? 
                    <div class="animated-icon1 float-right mt-1">
                        <span class="info-color"></span><span class="info-color"></span><span class="info-color"></span>
                    </div>
                    </h5>
                    </a>
                </div>
                <!-- Conteudo -->
                <div id="collapse5" class="collapse" role="tabpanel" aria-labelledby="heading5" data-parent="#accordionEx194">
                    <div class="card-body pt-3">
                    <p>Com a Classroom API, você pode realizar de forma programática muitas das ações que os 
                        professores e alunos podem fazer pela interface do usuário do Google Sala de aula. 
                        Por exemplo, você pode sincronizar com os sistemas de informações de alunos, 
                        ver todas as turmas de um domínio e gerenciar as atividades dos cursos.</p>
                    <p>Os serviços que não são do Google podem usar a Classroom API para integrar recursos ao 
                        Google Sala de aula. Por exemplo, um app pode permitir que um professor copie e 
                        reutilize uma turma do Google Sala de Aula em vez de recriá-la e adicionar novamente cada aluno. 
                        Os apps também podem ver, criar e modificar os trabalhos no Google Sala de Aula, 
                        adicionar materiais aos trabalhos, entregar as tarefas dos alunos e enviar as notas para o 
                        Google Sala de Aula de forma programática.</p>
                    </div>
                </div>
            </div>

            <!-- Item 3 -->
            <div class="card">
                <!-- Título -->
                <div class="card-header" role="tab" id="heading6">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx194" href="#collapse6" aria-expanded="false"
                    aria-controls="collapse6">
                    <h5 class="mb-0">
                    Como um app acessa os dados do Google Sala de Aula? 
                    <div class="animated-icon1 float-right mt-1">
                        <span class="info-color"></span><span class="info-color"></span><span class="info-color"></span>
                    </div>
                    </h5>
                    </a>
                </div>
                <!-- Conteúdo -->
                <div id="collapse6" class="collapse" role="tabpanel" aria-labelledby="heading6" data-parent="#accordionEx194">
                    <div class="card-body pt-3">
                    <p>Antes de poder acessar os dados do Google Sala de aula, o aplicativo ou serviço precisa solicitar 
                        a autorização do usuário do Google Sala de aula. O aplicativo solicita as permissões específicas 
                        necessárias, como nome de usuário, endereço de e-mail ou foto do perfil, e o usuário pode aprovar 
                        ou rejeitar a solicitação do serviço. Para autorizar a permissão, a Classroom API utiliza o 
                        padrão OAuth, comum na Internet.</p>
                    </div>
                </div>
            </div>
            
            <!-- Item 4 -->
            <div class="card">
                <!-- Título -->
                <div class="card-header" role="tab" id="heading7">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx194" href="#collapse7" aria-expanded="false"
                    aria-controls="collapse7">
                    <h5 class="mb-0">
                    O que a Classroom API faz pelos usuários? 
                    <div class="animated-icon1 float-right mt-1">
                        <span class="info-color"></span><span class="info-color"></span><span class="info-color"></span>
                    </div>
                    </h5>
                    </a>
                </div>
                <!-- Conteudo -->
                <div id="collapse7" class="collapse" role="tabpanel" aria-labelledby="heading7" data-parent="#accordionEx194">
                    <div class="card-body pt-3">
                    <p>As tarefas específicas que a Classroom API pode realizar dependem da função que o usuário 
                        exerce na turma. Assim como na interface do usuário do Google Sala de Aula, 
                        um usuário pode ser aluno, professor ou administrador. 
                        Os alunos e professores podem aprovar apps de terceiros e denunciar abuso.</p>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col">Se o usuário é um…</th>
                                    <th scope="col">A API pode…</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">aluno</th>
                                    <td>
                                        <ul>
                                            <li>Ver os detalhes do curso e os professores desse curso.</li>
                                        </ul>    
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">professor</th>
                                    <td>
                                        <ul>
                                            <li>
                                                Criar, ver ou excluir as turmas do professor.
                                            </li>
                                            <li>
                                                ver, adicionar ou remover alunos e professores adicionais das turmas dele;
                                            </li>
                                            <li>
                                                ver e devolver trabalhos, criar tarefas e tópicos e atribuir notas nas turmas dele.
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">administrador</th>
                                    <td>
                                        <ul>
                                            <li>
                                                Criar, ver ou excluir qualquer turma no domínio do G Suite for Education do administrador.
                                            </li>
                                            <li>
                                                adicionar ou remover alunos e professores de todas as turmas do domínio dele;
                                            </li>
                                            <li>
                                                ver os trabalhos e tópicos de todas as turmas do domínio dele.
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4 col-md-4">
        <br><br><br><br><br><br><br><br><br><br>
        <img src="<?= base_url('assets/images/laptop_g.jpg') ?>" class="img-fluid" alt="Laptop Google">
    </div>
</div>